package com.example.demo.api.service;

import java.util.ArrayList;

import com.example.demo.api.model.ApiModel;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ServiceApi {

	@GET(value = "/comments")
	Call<ArrayList<ApiModel>> findAll();
}
