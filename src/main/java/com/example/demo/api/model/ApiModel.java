package com.example.demo.api.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ApiModel {
	private Integer postId;
	private Integer id;
	private String name;
	private String email;
	private String body;
}
