package com.example.demo.api;

import com.example.demo.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

	public static Retrofit getClient() {
		Gson gson = new GsonBuilder().create();
		HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
		interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
		OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
		return new Retrofit.Builder().baseUrl(Constants.URL_BASE).addConverterFactory(GsonConverterFactory.create(gson))
				.client(client).build();

	}
}
