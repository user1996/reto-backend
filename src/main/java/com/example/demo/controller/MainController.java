package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Response;
import com.example.demo.service.impl.ApiServiceTransform;

@RestController
@RequestMapping("/post")
public class MainController {

	@Autowired
	ApiServiceTransform service;

	@PostMapping("/getAll")
	public Response list() {
		return service.listData();
	}
}
