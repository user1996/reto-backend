package com.example.demo.service;

import java.util.ArrayList;

import com.example.demo.api.model.ApiModel;


public interface IApiService {
	public ArrayList<ApiModel> listAll();
}
