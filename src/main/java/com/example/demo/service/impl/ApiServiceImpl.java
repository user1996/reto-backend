package com.example.demo.service.impl;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.example.demo.api.ApiClient;
import com.example.demo.api.model.ApiModel;
import com.example.demo.api.service.ServiceApi;
import com.example.demo.service.IApiService;

@Service
public class ApiServiceImpl implements IApiService {

	private ServiceApi api;

	@Override
	public ArrayList<ApiModel> listAll() {
		api = ApiClient.getClient().create(ServiceApi.class);
		try {
			return api.findAll().execute().body();
		} catch (Exception e) {
			return new ArrayList<>();
		}
	}

}
