package com.example.demo.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.api.model.ApiModel;
import com.example.demo.model.Response;
import com.example.demo.utils.Utils;

@Service
public class ApiServiceTransform {
	@Autowired
	private ApiServiceImpl service;

	public Response listData() {
		List<String> items = new ArrayList<>();
		for (ApiModel model : service.listAll()) {
			items.add(Utils.transform(model.getPostId(), model.getId(), model.getEmail()));
		}
		return new Response(items);
	}
}
