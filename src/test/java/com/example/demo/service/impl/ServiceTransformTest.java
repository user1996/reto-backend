package com.example.demo.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.example.demo.model.Response;
import com.example.demo.util.UtilsTest;

@RunWith(MockitoJUnitRunner.class)
public class ServiceTransformTest {

	@Mock
	ApiServiceTransform service;
	@Test
	public void listData() {
		when(service.listData()).thenReturn(UtilsTest.getResponse("transformResponse.json"));
		Response response = service.listData();
		assertThat(response.getData().size()).isEqualTo(500);
	}
}
