package com.example.demo.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.example.demo.api.model.ApiModel;
import com.example.demo.api.service.ServiceApi;
import com.example.demo.util.UtilsTest;

@RunWith(MockitoJUnitRunner.class)
public class ServiceImplTest {

	@Mock
	private ServiceApi api;
	
	@Mock
	private ApiServiceImpl service;
	
	@Test
	public void listAll() throws IOException {
		when(service.listAll()).thenReturn(UtilsTest.getResponseApi("responseApi.json"));
		ArrayList<ApiModel> response = service.listAll();
		assertThat(response.size()).isEqualTo(500);
	}
}
