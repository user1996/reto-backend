package com.example.demo.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import com.example.demo.utils.Utils;

@RunWith(MockitoJUnitRunner.class)
public class TestUtils {
	
	@Test
	public void transform() {
		String compare = "1|500|castillojose12031996@gmail.com";
		String res = Utils.transform(1, 500, "castillojose12031996@gmail.com");
		assertEquals(res, compare);
	}
}
