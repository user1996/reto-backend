package com.example.demo.util;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

import com.example.demo.api.model.ApiModel;
import com.example.demo.model.Response;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Resources;

public class UtilsTest {
	public static Response getResponse(String filename) {
		try {
			URL url = Resources.getResource("response/" + filename);
			File file = new File(url.toURI());
			ObjectMapper mapper = new ObjectMapper();
			return mapper.readValue(file, new TypeReference<Response>() {
			});
		} catch (Exception e) {
			return new Response(Arrays.asList("Error al leer archivo"));
		}
	}
	public static ArrayList<ApiModel> getResponseApi(String filename) {
		try {
			URL url = Resources.getResource("response/" + filename);
			File file = new File(url.toURI());
			ObjectMapper mapper = new ObjectMapper();
			return mapper.readValue(file, new TypeReference<ArrayList<ApiModel>>() {
			});
		} catch (Exception e) {
			return new ArrayList<>();
		}
	}
}
