package com.example.demo.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.example.demo.model.Response;
import com.example.demo.service.impl.ApiServiceTransform;
import com.example.demo.util.UtilsTest;

@RunWith(MockitoJUnitRunner.class)
public class ControllerTest {

	@InjectMocks
	MainController controller;
	
	@Mock
	ApiServiceTransform transform;

	@Test
	public void listAll() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
		when(transform.listData()).thenReturn(UtilsTest.getResponse("transformResponse.json"));
		Response response = controller.list();
		assertThat(response.getData().size()).isEqualTo(500);
	}
	
}
